-- This is section 3.4
-- 
-- data Vect : Nat -> Type -> Type where
--   Nil : Vect Z a
--   (::) : a -> Vect k a -> Vect (S k) a
  
vapp : Vect n a -> Vect m a -> Vect (n+m) a
vapp Nil ys = ys
vapp (x::xs) ys = x :: (xs `vapp` ys)
  
-- Finite Sets
-- 
-- This should be thought of as "the set of numbers up to n"
-- 
-- Silly Idris tutorial
data MyFin : Nat -> Type where
  MFZ : MyFin (S k)
  MFS : MyFin k -> MyFin (S k)

-- This is defined as "index" in the prelude
myIndex : {a : Type} -> {n: Nat} -> MyFin n -> Vect n a -> a
myIndex MFZ     (x :: xs) = x
myIndex (MFS k) (x :: xs) = myIndex k xs

-- "Using" notation
data Elem : a -> Vect n a -> Type where
  Here : {x:a} -> { xs : Vect n a } -> Elem x (x :: xs)
  There : {x, y : a} -> {xs : Vect n a} -> Elem x xs -> Elem x (y :: xs)

testVec : Vect 4 Int
testVec = 3 :: 4 :: 5 :: 6 :: Nil

inVect : Elem 5 testVec
inVect = There (There Here)

-- ^ This is all very difficult to read
  
using ( x : a
      , y : a
      , xs : Vect n a
      , a : Type
      , n : Nat
      )
  data MyElem : a -> Vect n a -> Type where
    MyHere  : MyElem x (x :: xs)
    MyThere : MyElem x xs -> MyElem x (y :: xs)
  
  -- These are mutual blocks, which allow two interdependent functions to be defined at once
mutual
  even : Nat -> Bool
  even Z = True
  even (S k ) = odd k
  
  odd : Nat -> Bool
  odd Z = False
  odd (S k) = even k
  

-- the so type
inBounds : Int -> Int -> Bool
inBounds x y = x >= 0 && x < 640 && y >= 0  && y < 480
  
drawPoint : (x : Int) -> (y : Int) -> So (inBounds x y) -> IO ()

record Person : Type where
  MkPerson : (name : String) -> (age : Int ) -> Person 
